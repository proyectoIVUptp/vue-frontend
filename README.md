# crud-postgres

> A Vue.js project with express

## Requirements
* Node
* NPM or YARN

### Install dependencies
``` bash
$ yarn install
```

### Build Static Setup
``` bash
# build for development
$ yarn run dev
```

### Build Server Setup
``` bash
# build for production with minification
$ yarn run build

# start sever
$ yarn start
```
