const express = require('express');
const router = express.Router();

router.get('/', (req, res) => {
    res.render('index', { title: "Insopesca | Persona" })
});

router.get('/:pk', (req, res) => {
    res.render('index', { title: "Insopesca | Detalles" })
})

module.exports = router;