const express = require('express');
const router = express.Router();
const person = require("./person.js")
router.get('/', (req, res) => {
    res.render('index', { title: "Insopesca" })
});

router.get('/login', (req, res) => {
    res.render('index', { title: "Insopesca | Iniciar Sesión" })
})

router.get('/dashboard', (req, res) => {
    res.render('index', { title: "Insopesca | Dashboard" })
})

router.get('/settings', (req, res) => {
    res.render('index', { title: "Insopesca | Configuración" })
})

router.get('/forgot-password', (req, res) => {
    res.render('index', { title: "Insopesca | Olvido Su Contraseña" })
})

router.use('/person', person)

router.use((req, res, next) => {
    res.status(404)
    if (req.accepts('html')) {
        res.render('404', { title: "Insopesca | 404" });
        return;
    }
});

module.exports = router;
