// import module
const express = require("express") 
const morgan = require('morgan');
const swig = require('swig');
const path = require('path');
// init modules express
const app = express();

app.use(morgan('dev'));

// Middlewares
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header('Access-Control-Allow-Methods', '*');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

//Config Templates...
const DIR_TEMPLATE = path.resolve(__dirname, './templates')
app.engine('html', swig.renderFile);
app.set('view engine', 'html');
app.set('views', DIR_TEMPLATE);

// static
const DIR_STATIC = path.resolve(__dirname, './public')
app.use('static', express.static(DIR_STATIC));
app.use(express.static(DIR_STATIC));

// routes
const fallback = require('express-history-api-fallback')
app.use(fallback('index.html', { root: DIR_TEMPLATE }))
// const router = require('./routes')
// app.use("/", router)

// start server
const HOST = '0.0.0.0';
const PORT = process.env.PORT || 3000;
app.set('port', PORT);
app.listen(PORT, () =>{
    console.log(`Running on http://${HOST}:${PORT}`);
})

// sudo sysctl fs.inotify.max_user_watches=582222 && sudo sysctl -p