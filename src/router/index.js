import Vue from 'vue';
import Router from 'vue-router';
import Account from '../methods/Account';
import Homepage from '../components/homepage/Homepage.vue';
import Login from '../components/login/Login.vue';
import ForgotPassword from '../components/forgotPassword/ForgotPassword.vue';
import Dashboard from '../components/dashboard/Dashboard.vue';
import Settings from '../components/settings/Settings.vue';
import Person from '../components/account/person/Person.vue';
import PersonAdd from "../components/account/person/PersonAdd.vue"
import PersonProfile from '../components/account/person/PersonProfile.vue';
import PersonList from '../components/account/person/PersonList.vue';
import Error404 from '../components/Error404.vue';

Vue.use(Router)

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'homepage',
      component: Homepage
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
      meta: {
        requireAuthLogin: true
      }
    },
    {
      path: '/forgot-password',
      name: 'forgotPassword',
      component: ForgotPassword,
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: Dashboard,
      meta: {
        requireAuth: true
      }
    },
    {
      path: '/settings',
      name: 'setting',
      component: Settings,
      meta: {
        requireAuth: true
      }
    },
    {
      path: "/person",
      component: Person,
      meta: {
        requireAuth: true
      },
      children: [
        {
          path: '/',
          component: PersonList
        },
        {
          path: 'profile/:pk',
          component: PersonProfile
        },
        {
          path: 'add',
          component: PersonAdd
        }
      ]
    },
    {
      path: '*',
      name: '404',
      component: Error404
    }
  ]
})

router.beforeEach((to, from, next) => {
  let body = document.querySelector("body")
  if (to.fullPath == "/" || to.fullPath == "/login") {
    body.classList.remove("opacity")
  } else {
    body.classList.add("opacity")
  }
  console.log(to)
  if (to.matched.some(record => record.meta.requireAuth)) {
    let token = localStorage.getItem('token');
    if (token == null) {
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      })
    }
  };
  if (to.matched.some(record => record.meta.requireAuthLogin)) {
    if (localStorage.getItem('token') != null) {
      next({
        path: '/dashboard',
        params: { nextUrl: to.fullPath }
      })
    }
  }
  next()
})

export default router
