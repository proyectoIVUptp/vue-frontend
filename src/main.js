require("materialize-css");
require("./scss/main.scss");

import Vue from 'vue'
import App from './App.vue'
import router from './router'
import init from './methods/init'

new Vue({
  router,
  el: '#app',
  render: h => h(App)
})
init();
