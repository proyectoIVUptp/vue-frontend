module.exports = function init() {
    const elems_side = document.querySelectorAll('.sidenav');
    if (elems_side) {
        let instances_side = M.Sidenav.init(elems_side, {
            draggable: true,
            menuWidth: 200,
            edge: 'right'
        });
    }

    const elems_parallax = document.querySelectorAll('.parallax');
    if (elems_parallax) {
        let instance_parallax = M.Parallax.init(elems_parallax);
    }

    const elem_select = document.querySelectorAll('.select');
    if (elem_select) {
        let instance_select = M.FormSelect.init(elem_select, {});
    }

    const elem_date = document.querySelectorAll('.datepicker');
    if (elem_date) {
        let instance_date = M.Datepicker.init(elem_date, { 'format': 'dd/mm/yyyy' });
    }

    const elem_tabs = document.querySelectorAll('.tabs');
    if (elem_tabs) {
        let instance_tabs = M.Tabs.init(elem_tabs, {
            onShow: true,
            // swipeable: true
        });
    }
    const elems = document.querySelectorAll('.modal');
    const instances = M.Modal.init(elems, {});
    
    let min = new Date;
    min.setFullYear(min.getFullYear() - 90);
    const elems_date = document.querySelectorAll('.datepicker');
    if (elems_date) {
        let instances_date = M.Datepicker.init(elems_date, {
            format: 'yyyy-mm-dd',
            i18n: {
                months: [ 'Enero', 'Febrero', 'Marzo', 'Abril', 
                            'Mayo', 'Junio', 'Julio', 'Agosto', 
                            'Septiembre', 'Octubre', 'Noviembre', 'Diciembre' ],
                monthsShort: [ 'Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 
                                'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic' ],
                weekdays: [ 'Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado' ],
                weekdaysShort: [ 'Dom', 'Lun', 'Mar', 'Mie', 'Jue', 'Vie', 'Sab' ],
                weekdaysAbbrev: [ 'D', 'L', 'M', 'MI', 'J', 'V', 'S' ],
                cancel: "Cancelar",
                today: 'Hoy',
                clear: 'Limpiar',
                close: 'Ok',
            },
            firstDay: 1,
            minDate: min,
        });
    }

    const inp = document.getElementsByTagName("input");
    const text = document.getElementsByTagName("textarea");
    const sel = document.getElementsByTagName("select");

    let list = Array();
    for (i=0; i < inp.length; i++){
        if(inp[i].type !=='file'){
            list.push(inp[i])
        }
    }
    lista = [list, text, sel];

    for (i=0; i < lista.length; i++){
        for (j=0; j < lista[i].length; j++){
            lista[i][j].addEventListener("blur", borrarEspacioMayus);
            lista[i][j].addEventListener("paste", noPaste);
        }
    }

    function borrarEspacioMayus(ev) {
        var txt = ev.srcElement.value.trim(), aux;
        do {
            aux = txt;
            txt = txt.replace("  ", " ");
        } while (txt !== aux);
        ev.srcElement.value = txt;
    };

    function noPaste(ev) {
        ev.preventDefault();
    };
}
