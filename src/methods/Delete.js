import swal from "sweetalert2";
import { eventBus } from "../helpers/EventBus";

const remove = () => {
    swal({
        title: '¿Esta Seguro de Eliminar?',
        text: "No Podra Revetir Esto Cambios",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: "#3085d6",
        cancelButtonColor: "#d33",
        confirmButtonText: "Si",
        cancelButtonText: "Volver",
        allowOutsideClick: false,
        allowEscapeKey: false
      }).then((result) => {
        if (result.value) {
          eventBus.$emit("delete", true)
        }
        eventBus.$emit("delete-user", false)
      })
}

export default remove;