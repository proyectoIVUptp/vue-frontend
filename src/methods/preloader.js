import swal from 'sweetalert2'

const preloader = () => {
    swal({
        title: 'Cargando....',
        // imageUrl: '/img/logo.png',
        // imageWidth: 400,
        // imageHeight: 400,
        // imageAlt: 'Insopesca',
        timer: 1000,
        showConfirmButton: false,
        // onOpen: () => {
        //     swal.showLoading()
        // }
    })
}

export default preloader
