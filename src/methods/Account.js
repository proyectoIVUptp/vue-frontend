import fetch from '../helpers/Fetch';

const getAccounts = async (token) => {
    try {
        return await fetch.getData('/account/list/', token)
    } catch (error) {
        console.log(`error en getAcocunts: \n ${error}`)
    }
}

const getAccount = async (token) => {
    try {
        return await fetch.getData(`/account/`, token)
    } catch (error) {
        console.log(`error en getAcocunt: \n ${error}`)
    }
}

const getAccountByPk = async (pk, token) => {
    try {
        return await fetch.getData(`/account/?pk=${pk}`, token)
    } catch (error) {
        console.log(`error en getAcocuntByPk: \n ${error}`)
    }
}

const addAccount = async (data, token) => {
    try {
        return await fetch.postData("/singup/", data, token)
    } catch (error) {
        console.log(`error en addAcocunt: \n ${error}`)
    }
}

const removeAccount = async (pk, token) => {
    try {
        return await fetch.deleteData("/account/", pk, token)
    } catch (error) {
        console.log(`error en removeAcocunt: \n ${error}`)
    }
}

const updateAccount = async (data, token) => {
    try {
        return await fetch.putData('/account/', data, token)
    } catch (error) {
        console.log(`error en updateAcocunt: \n ${error}`)
    }
}

const pageAccount = async (count, search, token) => {
    try {
        return await fetch.getData(
            `/account/list/?page=${count}&search=${search}`, token)
    } catch (error) {
        console.log(`error en pageAcocunts: \n ${error}`)
    }
}

const filteredAccount = async (search, token) => {
    try {
        return await fetch.getData(
            `/account/list/?search=${search}`, token)
    } catch (error) {
        console.log(`error en filteredAcocunts: \n ${error}`)
    }
}

const getHistoryLogin = async (pk, token) => {
    try {
        return await fetch.getData(
            `/history-login/?pk=${pk}`, token)
    } catch (error) {
        console.log(`error en filteredAcocunts: \n ${error}`)
    }
}

const account = {
    getAccounts,
    getAccount,
    getAccountByPk,
    addAccount,
    updateAccount,
    removeAccount,
    pageAccount,
    filteredAccount,
    getHistoryLogin
}

export default account;