require('es6-promise').polyfill();
require('isomorphic-fetch');

// const BASE_API = 'http://192.168.43.231:8000';
// const BASE_API = 'http://192.168.250.5:8000';
const BASE_API = 'http://10.42.0.1:8000';
// const BASE_API = 'http://127.0.0.1:8000';
const BASE_URL = `${BASE_API}/api`;

// POST method
module.exports.postData = (url = ``, data = {}, token = null) => {
    let headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    }

    if (token) headers.Authorization = `Token ${token}`;

    let myInit = {
        method: 'POST',
        withCredentials: true,
        headers: headers,
        credentials: "same-origin",
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify(data)
    };

    let myRequest = new Request(BASE_URL + url, myInit);

    return fetch(myRequest)
        .then(response => response.json())
        .catch(error =>  {
            throw new Error(`Fetch =`, error)
        });
};

// PUT method
module.exports.putData = (url = ``, data = {}, token = null) => {
    let headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    }

    if (token) headers.Authorization = `Token ${token}`;

    let myInit = {
        method: 'PUT',
        withCredentials: true,
        headers: headers,
        credentials: "same-origin",
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify(data)
    };

    let myRequest = new Request(BASE_URL + url, myInit);

    return fetch(myRequest)
        .then(response => response.json())
        .catch(error =>  {
            throw new Error(`Fetch =`, error)
        });
};

// GET method
module.exports.getData = (url = ``, token = null) => {
    let headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    }
    if (token) headers.Authorization = `Token ${token}`;

    let myInit = {
        method: 'GET',
        withCredentials: true,
        headers: headers,
        credentials: "same-origin",
        mode: 'cors',
        cache: 'default',
    };

    let myRequest = new Request(BASE_URL + url, myInit);

    return fetch(myRequest)
        .then(response => response.json())
        .catch(error =>  {
            throw new Error(`Fetch =`, error)
        });
};

module.exports.deleteData = (url = ``, data = {}, token = null) => {
    let headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    }

    if (token) headers.Authorization = `Token ${token}`;

    let myInit = {
        method: 'DELETE',
        withCredentials: true,
        headers: headers,
        credentials: "same-origin",
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify(data)
    };

    let myRequest = new Request(BASE_URL + url, myInit);

    return fetch(myRequest)
        .then(response => response.json())
        .catch(error =>  {
            throw new Error(`Fetch =`, error)
        });
};

module.exports.patchData = (url = ``, data = {}, token = null) => {
    let headers = {
        "Content-Type": "application/json; charset=utf-8",
        "Accept": "application/json"
    }

    if (token) headers.Authorization = `Token ${token}`;

    let myInit = {
        method: 'PATCH',
        withCredentials: true,
        headers: headers,
        credentials: "same-origin",
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify(data)
    };

    let myRequest = new Request(BASE_URL + url, myInit);

    return fetch(myRequest)
        .then(response => response.json())
        .catch(error =>  {
            throw new Error(`Fetch =`, error)
        });
};